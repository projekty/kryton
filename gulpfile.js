var gulp = require('gulp');
var sass = require('gulp-sass');
var sassGlob = require('gulp-sass-glob');
var autoprefixer = require('gulp-autoprefixer');

// sass
gulp.task('sass', function() {

    return gulp.src('./sass/*.scss')

        .pipe(sassGlob())
        .pipe(sass({
            compress: false
        }))
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(gulp.dest('./css'))
  
});

gulp.task('default', gulp.series('sass', function(){
   gulp.watch( './sass/**/*.scss', gulp.series('sass'));
}));

jQuery(document).ready(function($){

    // icon-menu
    $(".icon-menu").click(function() {
        if ($('body').hasClass('menu-open')) {
            $("body").removeClass("menu-open");
        }
        else {
            $("body").addClass("menu-open");
        }
    });

    // For collapsible menu in menu region
    $('#menu .block-menu ul > li:has(ul)').each(function() {
        $(this).append( "<span></span>" );
    });

    $('#menu .block-menu li.menu-item--active-trail > span').each(function() {
        $(this).prev().addClass("open");
        $(this).addClass("open");
    });

    $("#menu .block-menu span").click(function(){
        if($(this).hasClass('open')) {
            $(this).prev().removeClass("open");
            $(this).removeClass("open");
        } else {
            $(this).prev().addClass("open");
            $(this).addClass("open");
        }
    });
    
});
